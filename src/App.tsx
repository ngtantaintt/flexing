import {
  QueryCache,
  QueryClient,
  QueryClientProvider
} from '@tanstack/react-query';
import { ConfigProvider, ThemeConfig } from 'antd';
import React from 'react';
import RootRouter from './routes';

const theme: ThemeConfig = {
  token: {
    colorPrimary: '#24BCC6'
  }
};

const queryClient = new QueryClient({
  queryCache: new QueryCache({
    onError: (error: any) => {
      console.log(`Something went wrong: ${error?.message}`);
    }
  })
});

const App: React.FC = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <ConfigProvider theme={theme}>
        <RootRouter />
      </ConfigProvider>
    </QueryClientProvider>
  );
};

export default App;
