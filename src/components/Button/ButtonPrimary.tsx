import { mainColor } from '@/utilities/constant';
import React from 'react';
import styles from './style.module.scss';

interface IProps {
  text: string;
  backgroundColor?: boolean;
  onClick: (e?: any) => void;
}

const ButtonPrimary: React.FC<IProps> = ({
  text,
  onClick,
  backgroundColor
}: IProps) => {
  return (
    <button
      onClick={onClick}
      className={`${styles.buttonPrimary} buttonPrimary`}
      style={{
        backgroundColor: backgroundColor ? mainColor.teal300 : 'white',
        color: backgroundColor ? 'white' : mainColor.teal300
      }}
    >
      {text}
    </button>
  );
};

export default ButtonPrimary;
