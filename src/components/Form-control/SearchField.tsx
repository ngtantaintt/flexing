import { Controller } from 'react-hook-form';
import SearchInput from '../Input/Search';

interface IProps {
  name: string;
  control: any;
  placeholder: string;
}

// Using in form tag

const SearchField: React.FC<IProps> = ({
  name,
  control,
  placeholder
}: IProps) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field, formState }) => {
        return (
          <SearchInput
            placeholder={placeholder}
            value={field.value}
            onChange={(data: any) => field.onChange(data)}
          />
        );
      }}
    ></Controller>
  );
};
export default SearchField;
