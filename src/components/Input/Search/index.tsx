import { Input } from 'antd';
import React from 'react';
import * as Icons from 'react-feather';
import styles from './style.module.scss';

interface IProps {
  placeholder: string;
  value: string;
  onChange: (e?: any) => void;
}

const SearchInput: React.FC<IProps> = ({
  placeholder,
  value,
  onChange
}: IProps) => {
  return (
    <Input
      prefix={<Icons.Search />}
      placeholder={placeholder}
      className={styles.searchInput}
      value={value}
      onChange={onChange}
    />
  );
};

export default SearchInput;
