import { theme } from 'antd';
import { Link } from 'react-router-dom';

interface IProps {
  path: string;
  label: string;
}

const RouterLink: React.FC<IProps> = ({ path, label }: IProps) => {
  const {
    token: { colorPrimary }
  } = theme.useToken();

  return (
    <Link to={path} style={{ color: colorPrimary }}>
      {label}
    </Link>
  );
};

export default RouterLink;
