import { Icons } from '@/utilities';
import { Table, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { type TableProps as RcTableProps } from 'rc-table';
import './style.scss';

export interface IColumnType extends ColumnsType {
  title: string;
  dataIndex: string;
  key: string;
  width?: number;
  render?: React.ReactNode;
}

interface IProps {
  title: string;
  columns: IColumnType | any;
  dataSource: RcTableProps<any>['data'];
  loading: boolean;
  onRowDoubleClick?: false | ((row: any) => void);
}

const TableCustom: React.FC<IProps> = ({
  title,
  columns,
  dataSource,
  loading,
  onRowDoubleClick
}: IProps) => {
  return (
    <>
      <Typography className="titleTable">{title}</Typography>
      <Table
        columns={columns}
        dataSource={dataSource}
        loading={loading}
        locale={{ emptyText: <Icons.tick /> }}
        tableLayout="fixed"
        className="data-table"
        rowSelection={{
          onSelect: (e: any) => {
            console.log('🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀 e: ', e);
          }
        }}
        onRow={(row) => {
          return {
            onDoubleClick: () => {
              if (onRowDoubleClick) {
                onRowDoubleClick(row);
              }
              return;
            }
          };
        }}
      />
    </>
  );
};

export default TableCustom;
