import ButtonPrimary from '@/components/Button/ButtonPrimary';
import TableCustom from '@/components/Table';
import { ColumnsType } from 'antd/es/table';
import { useState } from 'react';

interface IProps {}

interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}

const data: DataType[] = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park, New York No. 1 Lake Park'
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 2 Lake Park, London No. 2 Lake Park'
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sydney No. 1 Lake Park, Sydney No. 1 Lake Park'
  }
];

const columns: ColumnsType<DataType> = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    width: 150
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
    width: 80
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address 1'
  },
  {
    title: 'Long Column Long Column Long Column',
    dataIndex: 'address',
    key: 'address 2'
  },
  {
    title: 'Long Column Long Column',
    dataIndex: 'address',
    key: 'address 3'
  },
  {
    title: 'Long Column',
    dataIndex: 'address',
    key: 'address 4'
  }
];

const FirstPage: React.FC<IProps> = ({}: IProps) => {
  const [loading, setLoading] = useState(false);

  const handleOnClick = (event: any) => {
    setLoading((prevLoading) => !prevLoading);
  };

  const handleOnDoubleClick = (row: any) => {
    console.log('🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀 row: ', row);
  };

  return (
    <>
      <ButtonPrimary text="Loading" onClick={handleOnClick} />
      <TableCustom
        columns={columns}
        loading={loading}
        dataSource={data}
        title="TESTTTTTTTTTT"
        onRowDoubleClick={handleOnDoubleClick}
      />
    </>
  );
};

export default FirstPage;
