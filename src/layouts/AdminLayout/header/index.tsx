import SearchInput from '@/components/Input/Search';
import useDebounce from '@/hooks/useDebounce';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { Header } from 'antd/es/layout/layout';
import React, { useEffect, useState } from 'react';
import './style.scss';

interface IProps {
  handleCollapsed: any;
  collapsed: boolean;
}

const HeaderLayout: React.FC<IProps> = ({
  handleCollapsed,
  collapsed
}: IProps) => {
  const [searchText, setSearchText] = useState('');

  const debouncedSearch = useDebounce(searchText, 500);

  const handleOnChange = (e: any) => {
    setSearchText(e.target.value);
  };

  useEffect(() => {
    if (debouncedSearch === '') return;
    // call API and query

    console.log(
      '🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀 debouncedSearch: ',
      debouncedSearch
    );
  }, [debouncedSearch]);

  return (
    <Header
      style={{
        height: '64px',
        padding: 0,
        backgroundColor: 'white'
      }}
    >
      <Button
        type="text"
        icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        onClick={handleCollapsed}
        style={{
          fontSize: '16px',
          width: 64,
          height: 64
        }}
      />
      <div
        style={{
          lineHeight: 'normal',
          display: 'flex',
          alignItems: 'center',
          marginRight: '1%'
        }}
      >
        <SearchInput
          placeholder="Seach Here"
          onChange={handleOnChange}
          value={searchText}
        />
      </div>
    </Header>
  );
};

export default HeaderLayout;
