import { Layout } from 'antd';
import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import HeaderLayout from './header';
import NavLayout from './nav';
import styles from './style.module.scss';

interface IProps {}

const AdminLayout: React.FC<IProps> = ({}: IProps) => {
  const [collapsed, setCollapsed] = useState(false);

  const handleCollapsed = () => {
    setCollapsed((prevCollapsed) => !prevCollapsed);
  };

  return (
    <Layout className={styles.container}>
      <NavLayout collapsed={collapsed} />
      <Layout>
        <HeaderLayout handleCollapsed={handleCollapsed} collapsed={collapsed} />
        <div className={styles.content}>
          <Outlet />
        </div>
      </Layout>
    </Layout>
  );
};

export default AdminLayout;
