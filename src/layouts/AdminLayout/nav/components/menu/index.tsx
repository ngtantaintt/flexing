import RouterLink from '@/components/RouterLink';
import { Menu, MenuProps } from 'antd';
import React from 'react';
import * as Icons from 'react-feather';
import './style.scss';

interface IProps {}

type MenuItem = Required<MenuProps>['items'][number];

const NavMenu: React.FC<IProps> = (props: IProps) => {
  const items: MenuItem[] = [
    {
      key: 'item-1',
      label: <RouterLink path="/dashboard" label="Dashboard" />,
      icon: <Icons.Home />,
      children: [
        {
          key: 'sub-item-1',
          label: (
            <RouterLink path="/dashboard" label="Dashboard Test Submenu" />
          ),
          icon: <Icons.Anchor />
        }
      ]
    }
  ];

  return <Menu mode="inline" items={items} style={{ userSelect: 'none' }} />;
};

export default NavMenu;
