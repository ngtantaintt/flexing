import Logo from '@/components/Logo';
import { theme } from 'antd';
import Sider from 'antd/es/layout/Sider';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import NavMenu from './components/menu';
import styles from './style.module.scss';
interface IProps {
  collapsed: boolean;
}

const NavLayout: React.FC<IProps> = ({ collapsed }: IProps) => {
  const {
    token: { colorPrimary }
  } = theme.useToken();
  const navigate = useNavigate();

  const styleSider: React.CSSProperties = {
    padding: '5px',
    backgroundColor: 'white'
  };

  const styleLogo: React.CSSProperties = {
    color: colorPrimary,
    fontFamily: 'Lalezar',
    fontSize: '22px',
    fontStyle: 'normal',
    fontWeight: 700,
    display: 'flex',
    alignItems: 'center',
    alignContent: 'space-between',
    justifyContent: 'space-evenly',
    transition: '2s ease'
  };

  const br = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="234"
      height="1"
      viewBox="0 0 234 1"
      fill="none"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.5 0.500061H233.75"
        stroke="url(#paint0_linear_47_276)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_47_276"
          x1="0.5"
          y1="0.500061"
          x2="231.5"
          y2="0.500061"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#E0E1E2" stopOpacity="0" />
          <stop offset="0.5" stopColor="#E0E1E2" />
          <stop offset="1" stopColor="#E0E1E2" stopOpacity="0.15625" />
        </linearGradient>
      </defs>
    </svg>
  );

  return (
    <Sider collapsed={collapsed} width="250px" style={styleSider}>
      <div
        style={styleLogo}
        className={styles.logo}
        onClick={() => navigate('/')}
      >
        <Logo />
        {!collapsed && 'MANAGE SALES'}
      </div>
      {!collapsed && br}
      <NavMenu />
    </Sider>
  );
};

export default NavLayout;
