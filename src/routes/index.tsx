import { FirstPage } from '@/containers';
import AdminLayout from '@/layouts/AdminLayout';
import { Route, Routes, useLocation } from 'react-router-dom';

interface IProps {}
const RootRouter: React.FC<IProps> = () => {
  const location = useLocation();

  const renderLayout = () => {
    return <AdminLayout />;
  };

  return (
    <Routes location={location}>
      <Route path="/" element={renderLayout()}>
        <Route path="dashboard" element={<FirstPage />} />
      </Route>
    </Routes>
  );
};

export default RootRouter;
