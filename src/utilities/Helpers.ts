import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";

export function numberWithComma(x: string | number, option?: {prefix?: string, suffix?: string}) {
    if(!x) return '-'
    return (option?.prefix || '') + `${x}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (option?.suffix || '');
}


export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;